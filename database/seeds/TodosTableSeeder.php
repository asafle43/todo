<?php

use Illuminate\Database\Seeder;

class TodosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('todos')->insert(
        [
            [
            'title' => 'Buy Milk',
            'user_id' => 1,
            'created_at' => date('Y-m-d G:i:s'),
            'author'=>'a',
            ],
            [
                'title' => 'Prepare for the test',
                'user_id' => 1,
                'created_at' => date('Y-m-d G:i:s'),
                'author'=>'b',
            ],
            [
                'title' => 'Read a book',
                'user_id' => 1,
                'created_at' => date('Y-m-d G:i:s'),
                'author'=>'h',
                
            ]

        ]);
    }
}


